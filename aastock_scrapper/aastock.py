import json

import requests
from bs4 import BeautifulSoup


class AASTOCK:

    _report_language_word_mapping = {'en': {'r': 'Recommend', 'p': 'Positive', 'n': 'Negative'},
                              ' tc': {'r': '推薦', 'p': '利好', 'n': '利淡'},
                              'sc': {'r': '推荐', 'p': '利好', 'n': '利淡'}
                              }

    def __init__(self):
        pass

